/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fdf.h                                            .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/18 15:14:46 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/14 11:49:48 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef		FDF_H
# define	FDF_H
#include "../minilibx/mlx.h"
#include "../libft/includes/libft.h"
#include "lib_mlx.h"
#include <math.h>
#include <time.h>
# define WIDTH 2300
# define HEIGHT 1300

typedef struct s_mlx_po
{
	int size;
	unsigned int	opa;
}				t_mlxpo;

typedef struct s_mlx_p
{
	int p_x;
	int p_y;
	int p_z;

	int r_x;
	int r_y;
	int r_z;

	int	scale;
}				t_mlxp;

typedef struct s_mlx
{
	void	*mlx_ptr;
	void	*win_ptr;
	void	*img_ptr;
	int		*addr;
	int		**data;
	int		data_x;
	int		data_y;
	int	width;
	int	height;
	int	bpp;
	int	sizeline;
	int	endian;
	t_mlxp	co;
	t_mlxkey	keybo;
	t_mlxmotion	motion;
	t_mlxmouse	mouse;
	t_mlxcord	cord;
	char		print;
}				t_mlx;

void	fdf_debug(t_mlx *st);
void	fdf_debug2(t_mlx *st);
int		get_key(int key, void *st);
int		get_mouse(int key, int x, int y, void *st);
int		fdf_destroy(void *tst);
void	ft_create(t_mlx *st);
void	fdf_error(t_mlx *st, char *str);
int		fdf_inittab(t_mlx *st, char *file);
int	motion(int x, int y, void *s);
void	fdf_draw(t_mlx *st);
void	draw_line(t_mlx *st, int x, int y, int h, int g, unsigned int dd);

void	draw_map(t_mlx *st);
#endif
