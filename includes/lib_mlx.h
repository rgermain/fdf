/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lib_mlx.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/11 11:38:09 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 10:04:02 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LIB_MLX_H
# define LIB_MLX_H
# include "../libft/includes/libft.h"
# include "mlx_keycode.h"

typedef struct	s_mlxpivot
{
	int x;
	int y;
}				t_mlxpivot;

typedef struct	s_mlxcord
{
	int x;
	int y;
	double	r_x;
	int	r_y;
	int	r_z;
	double	scale;
	t_mlxpivot	pivot;
}				t_mlxcord;

typedef struct	s_mlxline
{
	int x1;
	int y1;
	int x2;
	int y2;
	size_t	color;
}				t_mlxline;

typedef struct	s_mlxbr
{
	int	dx;
	int dy;
	int e;
}				t_mlx_br;

typedef struct	s_mlxkey
{
	char	key[280][1];
	char	print;
}				t_mlxkey;

typedef struct	s_mlxmotion
{
	int x;
	int y;
}				t_mlxmotion;

typedef struct	s_mlxmouse
{
	t_mlxmotion	motion;
	char		key[4][1];
	char		print;
}				t_mlxmouse;

typedef struct	s_mlxcircle
{
	int x;
	int y;
	int d;
	int xc;
	int yc;
	int r;
	size_t	color;
}				t_mlxcircle;

void			mlx_drawline(t_mlxline *st, void *mlx_ptr, void *win_ptr);
int				mlx_getkey(int key, void *s);
int				mlx_getmouse(int key, int x, int y, void *s);
int				mlx_getmotion(int x, int y, void *s);
int				mlx_destroy(void *s);
void			mlx_drawcircle(t_mlxcircle *st, void *mlx_ptr, void *win_ptr);
#endif
