/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mlx_key.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/11 12:18:23 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/26 19:09:18 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		getalt(t_mlx *st, int x, int y)
{
	return (0);
}

void	position(t_mlx *st, int x, int y, int *x2, int *y2, int j, int i)
{
	(*x2) = (cos(st->cord.r_x) * x) - (sin(st->cord.r_x) * y);
	(*y2) = (sin(st->cord.r_x) * x) - (cos(st->cord.r_x) * y);

/*	(*x2) += ((st->cord.x * 0.1) / (st->data[j][i] + 0.1));
	(*y2) += ((st->cord.y * 0.1) / (st->data[j][i] + 0.1));
*/

	(*x2) = st->cord.x + (((*x2) * st->cord.scale) / (st->data[i][j] + 0.1));
	(*y2) = st->cord.y + (((*y2) * st->cord.scale) / (st->data[i][j] + 0.1));
	ft_printf("dddddddddddddddddddddx2 = %d  y2 = %d\n", (*x2), (*y2));
}

void	draw_map(t_mlx *st)
{
	t_mlxcord co;
	t_mlxline	line;
	int i;
	int j;
	int ff = 10;

	i = 0;
	line.color = 0x00fffff;
	ft_bzero(&co, sizeof(t_mlxcord));
	co.y = st->cord.y;
	while ((i + 1) < st->data_y)
	{
		j = 0;
		co.x = st->cord.x;
		while ((j + 1) < st->data_x)
		{
			position(st, co.x, co.y, &(line.x1), &(line.y1), j, i);
			position(st, co.x + st->cord.scale, co.y, &(line.x2), &(line.y2), j, i);
			if ((j + 2) < st->data_y)
				mlx_drawline(&line, st->mlx_ptr, st->win_ptr);
			position(st, co.x, co.y, &(line.x1), &(line.y1), j,  i);
			position(st, co.x , co.y + st->cord.scale, &(line.x2), &(line.y2), j, i);
			if ((i + 2) < st->data_x)
				mlx_drawline(&line, st->mlx_ptr, st->win_ptr);
			j++;
			co.x += st->cord.scale;

		}
		co.y += st->cord.scale;
		i++;
	}
}
