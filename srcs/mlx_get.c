/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mlx_key.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/11 12:18:23 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/20 10:40:37 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		mlx_destroy(void *s)
{
	t_mlx	*st;
	
	st = (t_mlx*)s;
	mlx_destroy_window(st->mlx_ptr, st->win_ptr);
	exit (0);
}

int		mlx_getkey(int press, void *s)
{
	t_mlx	*st;
	
	st = (t_mlx*)s;
	st->keybo.key[press][0] = 1;
	if (st->keybo.key[ESC_KEY][0])
		mlx_destroy(st);
	if (st->keybo.key[NKPL_KEY][0])
		st->cord.r_x += 0.9;
	if (st->keybo.key[NKMN_KEY][0])
		st->cord.r_x -= 0.9;
	if (st->keybo.key[W_KEY][0])
		st->cord.x -= 25;
	if (st->keybo.key[S_KEY][0])
		st->cord.x += 25;
	if (st->keybo.key[A_KEY][0])
		st->cord.y -= 25;
	if (st->keybo.key[D_KEY][0])
		st->cord.y += 25;
	if (st->keybo.key[K1_KEY][0])
	{
		mlx_clear_window(st->mlx_ptr, st->win_ptr);
		st->keybo.key[K1_KEY][0] = 0;
	}
	st->keybo.key[press][0] = 0;
	mlx_clear_window(st->mlx_ptr, st->win_ptr);
	draw_map(st);
	mlx_do_sync(st->mlx_ptr);
	if (st->print)
	{
		ft_printf("%{T_BLUE}[mlx_getkey]%{T_EOC}\n");
		if (st->keybo.key[press][0])
			ft_printf("%{T_GREEN}  [key] = %d%{T_EOC}\n\n", press);
		else
			ft_printf("%{T_RED}  [key] = %d%{T_EOC}\n\n", press);
	}
	return (1);
}

int		mlx_getmouse(int press, int x, int y, void *s)
{
	t_mlx	*st;
	t_mlxline	line;
	t_mlxcircle circle;

	st = (t_mlx*)s;
	if (x < 0 || x > (int)st->width || y < 0 || y > (int)st->height)
	{
		ft_bzero(&(st->mouse.key), sizeof(st->mouse.key));
		return (0);
	}
	st->mouse.key[press][0] = (st->mouse.key[press][0] ? 0 : 1);
	st->mouse.motion.x = x;
	st->mouse.motion.y = y;
	if (st->mouse.key[SCROLLDOWN_KEY][0])
	{
		st->cord.scale += (st->cord.scale < 1500 ? 0.01 : 0.0);
		st->mouse.key[SCROLLDOWN_KEY][0] = 0;
	}
	else if (st->mouse.key[SCROLLUP_KEY][0])
	{
		st->cord.scale -= (st->cord.scale <= 1 ? 0.0 : 0.1);
		st->mouse.key[SCROLLUP_KEY][0] = 0;
	}
	ft_printf("scale = %d\n", st->cord.scale);
/*	if (st->mouse.key[BUT1_KEY][0])
	{
		if (st->keybo.key[Q_KEY][0])
		{
			circle.yc = y;
			circle.xc = x;
			circle.r = st->cord.scale;
			circle.color = rand();
			mlx_drawcircle(&circle, st->mlx_ptr, st->win_ptr);
			mlx_do_sync(st->mlx_ptr);
		}
	}
*/	
	if (st->print)
	{
		ft_printf("%{T_BLUE}[mlx_getmouse]%{T_EOC}\n");
		if (st->mouse.key[press][0])
			ft_printf("%{T_GREEN}  [key_mouse] = %d%{T_EOC}\n", press);
		else
			ft_printf("%{T_RED}  [key_mouse] = %d%{T_EOC}\n", press);
		ft_printf("   %{T_LGREY}[x = %7d ||  y = %7d]%{T_EOC}\n\n", x, y);
	}
	return (1);
}

int		mlx_getmotion(int x, int y, void *s)
{
	t_mlx	*st;
	t_mlxline	line;
	t_mlxcircle circle;

	st = (t_mlx*)s;
	if (x < 0 || x > (int)st->width || y < 0 || y > (int)st->height)
		return (0);
	if (st->mouse.key[BUT1_KEY][0])
	{
		if (st->keybo.key[W_KEY][0])
		{
			line.x1 = st->mouse.motion.x;
			line.y1 = st->mouse.motion.y;
			line.x2 = x;
			line.y2 = y;
			line.color = rand() * x * y;
			mlx_drawline(&line, st->mlx_ptr, st->win_ptr);
			mlx_do_sync(st->mlx_ptr);
		}
		if (st->keybo.key[T_KEY][0])
		{
			circle.yc = y;
			circle.xc = x;
			circle.r = st->cord.scale;
			circle.color = rand() * x * y;
			mlx_drawcircle(&circle, st->mlx_ptr, st->win_ptr);
			mlx_do_sync(st->mlx_ptr);
			st->keybo.key[T_KEY][0] = 0;
		}
	}
	if (st->print)
	{
		ft_printf("%{T_BLUE}[mlx_getmotion]%{T_EOC}\n");
		ft_printf("   [x = %7d ||  y = %7d]\n\n", x, y);
	}
	return (1);
}
