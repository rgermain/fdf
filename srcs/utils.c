/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utils.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/05 12:42:08 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/13 18:04:19 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	free_inttab(int **tab, int y)
{
	int i;

	i = 0;
	while (i < y)
		free(tab[i++]);
	//ft_memdel((void**)tab);
}

void	fdf_free(t_mlx *st)
{
	free_inttab(st->data, st->data_y);
}

void	fdf_error(t_mlx *st, char *str)
{
	fdf_free(st);
	ft_printf("%1@", "error", "fdf", str);
}
