/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/18 15:24:16 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/14 12:55:37 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

int		fdf_counttab(char *str)
{
	int i;
	int	len;

	i = 0;
	len = 0;
	while (str[i] != 0)
	{
		while (str[i] == ' ')
			i++;
		if (str[i] == '-' || ft_isdigit(str[i]))
			len++;
		while (str[i] == '-' || ft_isdigit(str[i]))
			i++;
	}
	return (len);
}

int 	*fdf_inittab2(t_mlx *st, int fd)
{
	int		*data;
	char	*line;
	int		i;
	int		j;

	line = NULL;
	i = 0;
	j = 0;
	if (get_next_line(fd, &line) == 1)
	{
		st->data_x = fdf_counttab(line);
		if (!(data = (int*)malloc(sizeof(int) * st->data_x)))
		{
			ft_memdel((void**)&line);
			return (0);
		}
		while (i < st->data_x)
		{
			while (line[j] == ' ')
				j++;
			data[i++] = ft_atoi(line + j);
			while (line[j] == '-' || ft_isdigit(line[j]))
				j++;
		}
	}
	if (line)
		ft_memdel((void**)&line);
	return (data);
}

int		fdf_inittab1(t_mlx *st, int fd, int len)
{
	int	i;

	i = 0;
	st->data_y = len;
	if (!(st->data = (int **)malloc(sizeof(int*) * len)))
		fdf_error(st, "fdf_inittab");
	while (i < len)
	{
		if (!(st->data[i++] = fdf_inittab2(st, fd)))
		{
			ft_freetabi(st->data, sizeof(int) * len * len);
			fdf_error(st, "fdf_inittab");
		}
	}
	close(fd);
	return (1);
}

int		fdf_inittab(t_mlx *st, char *file)
{
	int 	fd;
	int		ret;
	char	buff[51];
	int		len;

	len = 0;
	if ((fd = open(file, O_RDONLY)) < 0)
		return (0);
	while ((ret = read(fd, buff, 50)))
	{
		buff[ret] = 0;
		ret = 0;
		while (buff[ret] != '\0')
		{
			if (buff[ret] == '\n')
				len++;
			ret++;
		}
	}
	close(fd);
	if ((fd = open(file, O_RDONLY)) < 0)
		return (0);
	if (!(fdf_inittab1(st, fd, len)))
		return (0);
	return (1);
}
