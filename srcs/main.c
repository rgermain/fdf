/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/18 15:24:16 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/26 19:10:14 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	create_win(t_mlx *st)
{
	if (!(st->mlx_ptr = mlx_init()))
		fdf_error(st, "create mlx_ptr");
	if (!(st->win_ptr = mlx_new_window(st->mlx_ptr, st->width, st->height, "fdf")))
		fdf_error(st, "create win_ptr");
	mlx_do_key_autorepeaton(st->mlx_ptr);
	mlx_hook(st->win_ptr, 2, 0, mlx_getkey, st);
	mlx_hook(st->win_ptr, 4, 0, mlx_getmouse, st);
	mlx_hook(st->win_ptr, 5, 0, mlx_getmouse, st);
	mlx_hook(st->win_ptr, 6, 0, mlx_getmotion, st);
	mlx_hook(st->win_ptr, 17, 0, mlx_destroy, st);
	mlx_loop(st->mlx_ptr);
}

void	main_manager(char **argv)
{
	t_mlx	st;

	ft_bzero(&st, sizeof(t_mlx));
	if (!(fdf_inittab(&st, argv[0])))
		return ;
	st.width = WIDTH;
	st.height = HEIGHT;
	st.print = 1;
	st.cord.scale = 8;
	st.cord.x = WIDTH / 4;
	st.cord.y = HEIGHT / 4;
	st.cord.r_x = 0.0;
	st.cord.pivot.x = st.width / 2;
	st.cord.pivot.y = st.height / 2;
	create_win(&st);
}

int	main(int argc, char **argv)
{
//	if (argc == 2)
		main_manager((argv + 1));
	return (0);
}
