/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mlx_drawcircle.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/12 12:11:47 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/13 18:37:09 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fdf.h"

void	mlx_drawcircle(t_mlxcircle *st, void *mlx_ptr, void *win_ptr)
{
	st->x = 0;
	st->y = st->r;
	st->d = st->r - 1;
	while (st->y >= st->x)
	{
/*		st->addr[((st->xc + st->x) * st->width) + st->yc + st->y] = st->color;
		st->addr[((st->xc + st->y) * st->width) + st->yc + st->x] = st->color;
		st->addr[((st->xc - st->x) * st->width) + st->yc + st->y] = st->color;
		st->addr[((st->xc - st->y) * st->width) + st->yc + st->x] = st->color;

		st->addr[((st->xc + st->x) * st->width) + st->yc - st->y] = st->color;
		st->addr[((st->xc + st->y) * st->width) + st->yc - st->x] = st->color;
		st->addr[((st->xc - st->x) * st->width) + st->yc - st->y] = st->color;
		st->addr[((st->xc - st->y) * st->width) + st->yc - st->x] = st->color;
*/
		mlx_pixel_put(mlx_ptr, win_ptr, (st->xc + st->x), st->yc + st->y, st->color);
		mlx_pixel_put(mlx_ptr, win_ptr, (st->xc + st->y), st->yc + st->x, st->color);
		mlx_pixel_put(mlx_ptr, win_ptr,(st->xc - st->x), st->yc + st->y, st->color);
		mlx_pixel_put(mlx_ptr, win_ptr, (st->xc - st->y), st->yc + st->x, st->color);

		mlx_pixel_put(mlx_ptr, win_ptr, (st->xc + st->x), st->yc - st->y, st->color);
		mlx_pixel_put(mlx_ptr, win_ptr,(st->xc + st->y), st->yc - st->x, st->color);
		mlx_pixel_put(mlx_ptr, win_ptr,(st->xc - st->x), st->yc - st->y, st->color);
		mlx_pixel_put(mlx_ptr, win_ptr,(st->xc - st->y), st->yc - st->x, st->color);
	
		if (st->d >= (2 * st->x))
			st->d -= (2 * st->x++) + 1;
		else if (st->d < (2 * (st->r - st->y)))
			st->d += (2 * st->y--) - 1;
		else
			st->d += (2 * (st->y-- - st->x++ - 1));
	}
}
